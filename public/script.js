function onClick() {
	  var f1 = parseInt(document.getElementById("field1").value);
	  var f2 = parseInt(document.getElementById("field2").value);
	  var re = /\d+$/;
	  if(re.test(f1)&&re.test(f2))
	  {
	  document.getElementById("result").innerHTML = f1 * f2;
	  }
	  else{
		  alert("Введены неверные данные!");
		  document.getElementById("result").innerHTML = "error";
	  }
	}
	
	window.addEventListener('DOMContentLoaded', function (event) {
		  console.log("DOM fully loaded and parsed");
		  let b = document.getElementById("calc_but");
		  b.addEventListener("click", onClick);
	});